= docker.sock

//tag::abstract[]

Mounting docker.sock file in a container allows the container to run new (privileged) containers
on the host and take full control of the host.

//end::abstract[]

If docker.sock is mounted inside a container, 
the container can use it to communicate with the host Docker daemon.
The container can listen to docker event, start/stop containers or run a new container
using privileged mode. 
A malicious container or a malicious/vulnerable program inside a container that has `docker.sock`
access can eventually take full control of the host.

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.

image::https://gitlab.com/insecure-programming/docker/sock/badges/master/pipeline.svg[]

=== Task 0

*Fork* and clone this repository.
Install `docker`, `docker-compose` and `make` on your system.

. Run unit tests: `make test`. 
. Build the program: `make build`.
. Run it: `make run`.
. Run security tests: `make securitytest`.

Note: The last test will fail. 


=== Task 1

Review `Dockerfile` and `docker-compose.yml` file. 
Why all the host devices (content of `/dev`) is printed out?
Find out the security issue and why tests fails.

Note: Avoid looking at tests and try to
spot the vulnerability on your own.

=== Task 2

The security weakness is mounting `docker.sock` in the container .
Edit `docker-compose.yml` and remove the mount point.

=== Task 3

Remove the `docker.sock` mount volume. This is an insecure practice. 
Run security tests again. Make sure build pass.
If you stuck, move to the next task.

=== Task 4 
 
Check out the `patch` branch and review the chages. 
Run all tests and make sure everything pass. 
 
=== Task 5 
 
Merge the patch branch to master. 
Commit and push your changes. 
Does pipeline for your forked repository show that you have passed the build?   

(Note: you do NOT need to send a pull request)

//end:lab[]

//tag::references[]

== References

//end::references[]
